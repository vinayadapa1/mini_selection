<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Route::get('/selection', [App\Http\Controllers\HomeController::class, 'index'])->name('selection');
//Route::get('/users', [App\Http\Controllers\UserController::class, 'index'])->name('users');

// Masters
//Route::get('/sales-employee', [App\Http\Controllers\SalesEmployeeController::class, 'index'])->name('sales-employee');
//Route::any('/sales-employee/add', [App\Http\Controllers\SalesEmployeeController::class, 'add'])->name('add-sales-employee');
//Route::get('/client', [App\Http\Controllers\ClientController::class, 'index'])->name('client');
//Route::get('/consultant', [App\Http\Controllers\ConsultantController::class, 'index'])->name('consultant');
//Route::get('/contractor', [App\Http\Controllers\ContractorController::class, 'index'])->name('contractor');

//Route::resource('sales-employee', 'SalesEmployeeController');
Route::get('/selection', [App\Http\Controllers\SelectionController::class, 'index']);
Route::post('/generate-report', [App\Http\Controllers\SelectionController::class, 'generateReport']);
Route::post('/add-specification', [App\Http\Controllers\SelectionController::class, 'addSpecification']);
Route::get('/specifications/{id}', [App\Http\Controllers\SelectionController::class, 'getSpecification']);
Route::resource('sales-employee', App\Http\Controllers\SalesEmployeeController::class);
Route::resource('clients', App\Http\Controllers\ClientController::class);
Route::resource('consultants', App\Http\Controllers\ConsultantController::class);
Route::resource('contractors', App\Http\Controllers\ContractorController::class);
Route::resource('users', App\Http\Controllers\UserController::class);


Route::get('/air-to-water', function () {
    return view('selection.air_to_water');
});

Route::get('/water-to-water', function () {
    return view('selection.water_to_water');
});

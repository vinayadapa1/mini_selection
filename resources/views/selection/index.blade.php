@php $page = 'selection'; @endphp
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img//apple-icon.png">
  <link rel="icon" type="image/png" href="{{ asset('public/img//favicon.png') }}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="_token" content="{{csrf_token()}}" />
  <title>
    Kehems | Selection
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('public/css/paper-dashboard.css?v=2.1.1') }}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{ asset('public/demo/demo.css') }}" rel="stylesheet" />

    <style>
    .error {
      color: #dc3545 !important;
    }
  </style>

</head>

<body class="">
  <div class="wrapper ">
    @include('layouts.sidebar');
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-minimize">
              <button id="minimizeSidebar" class="btn btn-icon btn-round">
                <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
                <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
              </button>
            </div>
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="javascript:;">Selection</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <!--<div class="collapse navbar-collapse justify-content-end" id="navigation">
            <form>
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <i class="nc-icon nc-zoom-split"></i>
                  </div>
                </div>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link btn-magnify" href="javascript:;">
                  <i class="nc-icon nc-layout-11"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Stats</span>
                  </p>
                </a>
              </li>
              <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="nc-icon nc-bell-55"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <a class="dropdown-item" href="#">Something else here</a>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link btn-rotate" href="javascript:;">
                  <i class="nc-icon nc-settings-gear-65"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Account</span>
                  </p>
                </a>
              </li>
            </ul>
          </div>-->
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          
          <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                <h5 class="title">Selection Criteria</h5>
              </div>
              <div class="card-body">
                <form id="report-form">
                                     
                  <label>Application Type<sup class="error">*</sup></label>
                  <div class="row">
                    <div class="col-md-4 pr-1">
                      <div class="form-group">
                        <div class="form-check-radio">
                         <label class="form-check-label">
                          <input class="form-check-input" type="radio" name="ip_application_type" value="aw" checked>
                          Air to Water
                          <span class="form-check-sign"></span>
                         </label>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4 pr-1">
                      <div class="form-group">
                        <div class="form-check-radio">
                         <label class="form-check-label">
                          <input class="form-check-input" type="radio" name="ip_application_type" value="ww">
                          Water to Water
                          <span class="form-check-sign"></span>
                         </label>
                        </div>
                      </div>
                    </div>

                  </div>


                  <div class="row">
                    <div class="col-md-3 pr-1">
                      <div class="form-group">
                        <label class="temp_text">Ambient Temp.<sup class="error">*</sup></label>
                        <input type="text" class="form-control" name="ip_ambient_temperature" value="25">
                      </div>
                    </div>
                    <div class="col-md-3 px-1">
                      <div class="form-group">
                        <label>Outlet Temp<sup class="error">*</sup>.</label>
                        <input type="text" class="form-control" name="ip_output_temperature" value="55">
                      </div>
                    </div>
                    <div class="col-md-3 pl-1">
                      <div class="form-group">
                        <label>Ltr/hr - From</label>
                        <input type="text" class="form-control" name="ip_litres_per_day_range_from">
                      </div>
                    </div>

                    <div class="col-md-3 pl-1">
                      <div class="form-group">
                        <label>To</label>
                        <input type="text" class="form-control" name="ip_litres_per_day_range_to">
                      </div>
                    </div>

                  </div>
                  
                  <div class="row">
                    <div class="col-md-3 pr-1">
                      <div class="form-group">
                        <label>Pump Head</label>
                           <select class="form-control" name="ip_pump_head_operation">
                           <option value=""></option>
                             <option value="<="><=</option>
                             <option value=">=">>=</option>
                           </select>                        
                      </div>
                    </div>

                    <div class="col-md-3 pr-1">
                      <div class="form-group">
                        <label>&nbsp;</label>
                        <input type="text" class="form-control" name="ip_pump_head" value="">
                      </div>
                    </div>

                    <div class="col-md-6 pl-1">
                      <div class="form-group">
                        <label>Refrigerant</label>
                           <select class="form-control" name="ip_refrigerant">
                             <option value=""></option>
                             <option value="R134A">R134A</option>
                             <option value="R410 A">R410 A</option>
                           </select>                        
                      </div>
                    </div>
                  </div>
                  
                  <button type="button" class="btn btn-primary btn-sm" id="search">Search</button>

                </form>
              </div>
            </div>
          </div>
          

           <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                <h5 class="title">Project Details</h5>
              </div>
              <div class="card-body">
                <form>
                
                  <div class="row">
                    <div class="col-md-4 pr-1">
                      <div class="form-group">
                        <label>Name of the Project</label>
                        <input type="text" class="form-control" placeholder="" value="">
                      </div>
                    </div>
                    <div class="col-md-4 px-1">
                      <div class="form-group">
                        <label>Location</label>
                        <input type="text" class="form-control" placeholder="" value="">
                      </div>
                    </div>
                    <div class="col-md-4 px-1">
                      <div class="form-group">
                        <label>Client</label>
                           <select class="form-control" name="client">
                             <option value=""></option>
                             @foreach($client_details as $client)
                             <option value="{{ $client->id }}">{{ $client->first_name. ' '.$client->last_name.', '.$client->city }}</option>
                             @endforeach
                           </select>
                      </div>
                    </div>
                  </div>
                  

                  <div class="row">
                    <div class="col-md-4 pr-1">
                      <div class="form-group">
                        <label>Nature of Client</label>
                           <select class="form-control" name="cars" id="cars">
                             <option value=""></option>
                             <option value="Direct Client">Direct Client</option>
                             <option value="Chanel Partner">Chanel Partner</option>
                           </select>
                      </div>
                    </div>
                    <div class="col-md-4 px-1">
                      <div class="form-group">
                        <label>Consultant</label>
                           <select class="form-control" name="consultant">
                             <option value=""></option>
                             @foreach($consultant_details as $consultant)
                             <option value="{{ $consultant->id }}">{{ $consultant->first_name.' '.$consultant->last_name }}</option>
                             @endforeach
                           </select>
                      </div>
                    </div>
                    <div class="col-md-4 px-1">
                      <div class="form-group">
                        <label>Contractor</label>
                           <select class="form-control" name="contractor">
                             <option value=""></option>
                             @foreach($contractor_details as $contractor)
                             <option value="{{ $contractor->id }}">{{ $contractor->first_name.' '.$contractor->last_name }}</option>
                             @endforeach
                           </select>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-8 pr-1">
                      <div class="form-group">
                        <label>Sales person</label>
                           <select class="form-control" name="employee">
                             <option value=""></option>
                             @foreach($employee_details as $employee)
                             <option value="{{ $employee->id }}">{{ $employee->first_name.' '.$employee->last_name }}</option>
                             @endforeach
                           </select>
                      </div>
                    </div>
                    <div class="col-md-4 px-1">
                      <div class="form-group">
                        <label>Region</label>
                           <select class="form-control" name="cars" id="cars">
                           <option value=""></option>
                             <option value="West">West</option>
                             <option value="Central">Central</option>
                             <option value="East">East</option>
                             <option value="North">North</option>
                             <option value="South">South</option>
                           </select>
                      </div>
                    </div>
                  </div>

                  
                </form>
              </div>
            </div>
          </div>
          
                    
        </div>
        
<div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Models</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                    <table class="table model-table">
                        <thead class="text-primary">
                            <tr>
                                <th>Name</th>
                                <th>Heating cap.</th>
                                <th>Ab. power</th>
                                <th>Cooling cap.</th>
                                <th>Pump head</th>
                                <th>Refrigerant</th>
                                <th>Litre/hr</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
      <footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
          <div class="row">
            <!--<nav class="footer-nav">
              <ul>
                <li><a href="https://www.creative-tim.com" target="_blank">Creative Tim</a></li>
                <li><a href="https://www.creative-tim.com/blog" target="_blank">Blog</a></li>
                <li><a href="https://www.creative-tim.com/license" target="_blank">Licenses</a></li>
              </ul>
            </nav>-->
            <div class="credits ml-auto">
              <span class="copyright">
                © <script>
                  document.write(new Date().getFullYear())
                </script>, kehems.com
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="{{ asset('public/js/core/jquery.min.js') }}"></script>
  <script src="{{ asset('public/js/core/popper.min.js') }}"></script>
  <script src="{{ asset('public/js/core/bootstrap.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/moment.min.js') }}"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="{{ asset('public/js/plugins/bootstrap-switch.js') }}"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="{{ asset('public/js/plugins/sweetalert2.min.js') }}"></script>
  <!-- Forms Validations Plugin -->
  <script src="{{ asset('public/js/plugins/jquery.validate.min.js') }}"></script>
  <!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="{{ asset('public/js/plugins/jquery.bootstrap-wizard.js') }}"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="{{ asset('public/js/plugins/bootstrap-selectpicker.js') }}"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="{{ asset('public/js/plugins/bootstrap-datetimepicker.js') }}"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
  <script src="{{ asset('public/js/plugins/jquery.dataTables.min.js') }}"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="{{ asset('public/js/plugins/bootstrap-tagsinput.js') }}"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="{{ asset('public/js/plugins/jasny-bootstrap.min.js') }}"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="{{ asset('public/js/plugins/fullcalendar/fullcalendar.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/fullcalendar/daygrid.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/fullcalendar/timegrid.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/fullcalendar/interaction.min.js') }}"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="{{ asset('public/js/plugins/jquery-jvectormap.js') }}"></script>
  <!--  Plugin for the Bootstrap Table -->
  <script src="{{ asset('public/js/plugins/nouislider.min.js') }}"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="{{ asset('public/js/plugins/chartjs.min.js') }}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{ asset('public/js/plugins/bootstrap-notify.js') }}"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('public/js/paper-dashboard.min.js?v=2.1.1') }}" type="text/javascript"></script><!-- Paper Dashboard DEMO methods, don't include it in your project! -->
  <script src="{{ asset('public/demo/demo.js') }}"></script>
  <script>
    $(document).ready(function() {

        $("input[name='ip_application_type']").click(function(){
          
          var application_type = $("input[name='ip_application_type']:checked").val()

          if(application_type == 'aw'){
            $(".temp_text").html('Ambient Temp.<sup class="error">*</sup>');
          }else{
            $(".temp_text").html('Chilled water outlet Temp.<sup class="error">*</sup>');
          }

        });
          
        

        $("#report-form").validate({
            rules: {
                ip_application_type : "required",
                ip_ambient_temperature : "required",
                ip_output_temperature : "required",
            },
            messages: {
                ip_application_type : "Required",
                ip_ambient_temperature : "Required",
                ip_output_temperature : "Required"
            }
        });

      
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('#search').click(function(){

            if($("#report-form").valid()){

            var ip_application_type = $("input[name='ip_application_type']:checked").val();
            var ip_ambient_temperature = $("input[name='ip_ambient_temperature']").val();
            var ip_output_temperature = $("input[name='ip_output_temperature']").val();
            var ip_litres_per_day_range_from = $("input[name='ip_litres_per_day_range_from']").val();
            var ip_litres_per_day_range_to = $("input[name='ip_litres_per_day_range_to']").val();
            var ip_pump_head_operation = $("select[name='ip_pump_head_operation']").val();
            var ip_pump_head = $("input[name='ip_pump_head']").val();
            var ip_refrigerant = $("select[name='ip_refrigerant']").val();

            $.ajax({
                url: "{{ url('/generate-report') }}",
                method: 'post',
                data: {
                    ip_application_type : ip_application_type,
                    ip_ambient_temperature : ip_ambient_temperature,
                    ip_output_temperature : ip_output_temperature,
                    ip_litres_per_day_range_from : ip_litres_per_day_range_from,
                    ip_litres_per_day_range_to : ip_litres_per_day_range_to,
                    ip_pump_head_operation : ip_pump_head_operation,
                    ip_pump_head : ip_pump_head,
                    ip_refrigerant : ip_refrigerant
                },
                success: function(data){
                    console.log(data.report_details);
                    $(".model-table tbody").empty();
                    var report_details = '';
                    if(data.report_details.length == 0){
                        report_details += '<tr><td colspan="8">There is no any Model</td></tr>';
                    }else{
                        for(var i=0; i<data.report_details.length; i++){
                            report_details += '<tr>';
                            report_details += ' <td hidden class="model_id">'+data.report_details[i]['model_id']+'</td>';
                            report_details += ' <td class="model_name">'+data.report_details[i]['model_name']+'</td>';
                            report_details += ' <td class="heating_capacity_pc">'+data.report_details[i]['heating_capacity_pc']+'</td>';
                            report_details += ' <td class="compressor_absorbed_power_pa">'+data.report_details[i]['compressor_absorbed_power_pa']+'</td>';
                            report_details += ' <td>'+data.report_details[i]['cooling_capacity_pf']+'</td>';
                            report_details += ' <td>'+data.report_details[i]['hot_hot_water_pump_head']+'</td>';
                            report_details += ' <td>'+data.report_details[i]['gen_refrigerant']+'</td>';
                            report_details += ' <td>'+data.report_details[i]['hot_water_quantity_l_per_day_calc']+'</td>';
                            report_details += ' <td class="text-right">';
                            report_details += '     <button type="button" rel="tooltip" class="btn btn-info btn-icon btn-sm view_model" data-id="'+data.report_details[i]['model_id']+'" title="View">';
                            report_details += '         <i class="fa fa-file"></i>';
                            report_details += '     </button>';
                            report_details += ' </td>';
                            report_details += '</tr>';
                        }
                    }
                    
                    $(".model-table tbody").append(report_details);
                    
                    /*$.each(data.errors, function(key, value){
                  			$('.alert-danger').show();
                  			$('.alert-danger').append('<p>'+value+'</p>');
                  		});*/
                }
                    
            });
            }
        });

        $(document).on("click", ".view_model", function(){
            /*if($("input[name='ip_application_type']:checked").val() == 'aw'){
                window.open("{{ url('/air-to-water') }}", "_blank");
            }else{
                window.open("{{ url('/water-to-water') }}", "_blank");
            }*/

            var user_id = "{{ auth()->user()->id }}";
            var user_type = "{{ auth()->user()->type }}";

            var application_type = $("input[name='ip_application_type']:checked").val();

            var ip_ambient_temperature = $("input[name='ip_ambient_temperature']").val();
            var ip_output_temperature = $("input[name='ip_output_temperature']").val();

            var client_id = $("select[name='client'] option:selected").val();
            var client_name = $("select[name='client'] option:selected").text();
            var consultant_id = $("select[name='consultant'] option:selected").val();
            var consultant_name = $("select[name='consultant'] option:selected").text();
            var contractor_id = $("select[name='contractor'] option:selected").val();
            var contractor_name = $("select[name='contractor'] option:selected").text();
            var employee_id = $("select[name='employee'] option:selected").val();
            var employee_name = $("select[name='employee'] option:selected").text();

            var model_id = $(this).parent().siblings(".model_id").text();
            //var model_name = $(this).parent().siblings(".model_name").text();
            var heating_capacity_pc = $(this).parent().siblings(".heating_capacity_pc").text();
            var compressor_absorbed_power_pa = $(this).parent().siblings(".compressor_absorbed_power_pa").text();

            $.ajax({
                url: "{{ url('/add-specification') }}",
                method: 'post',
                data: {
                  user_id : user_id,
                  user_type : user_type,
                  application_type : application_type,
                  ip_ambient_temperature : ip_ambient_temperature,
                  ip_output_temperature : ip_output_temperature,
                  client_id : client_id,
                  client_name : client_name,
                  consultant_id : consultant_id,
                  consultant_name : consultant_name,
                  contractor_id : contractor_id,
                  contractor_name : contractor_name,
                  employee_id : employee_id,
                  employee_name : employee_name,
                  model_id : model_id,
                  //model_name : model_name,
                  heating_capacity_pc : heating_capacity_pc,
                  compressor_absorbed_power_pa : compressor_absorbed_power_pa
                },
                success: function(data){
                  //console.log(data.key);
                  var key = data.key;
                  window.open("/specifications/"+key);
                  //console.log();
                  //window.open(url);
                }
            });  

        });

    });
  </script>
</body>

</html>


<!DOCTYPE HTML>
<!--
   Editorial by HTML5 UP
   html5up.net | @ajlkn
   Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
   <head>
      <title>Kehems Technologies</title>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
      <link rel="shortcut icon" href="http://miniselection.kehems.in/public/img/fevicon.png" />
      <link rel="stylesheet" href="http://miniselection.kehems.in/public/css/spec-css/main-modified.css" />
<style>

   
   .display-text {
      font-size: 1em;
   }
   
   .small_heading {
      font-size: 1.5em;
      line-height: 1;
   }      
   .top_heading {
      font-size: 2em;
      line-height: 1;
      margin: auto;
      padding: 15px
   }      
   
   .specs-header-v {
      background-color: none;
      font-size: 1em;
      font-weight: bold;
      border-bottom: 1px solid #aaa;
   }

   .prod-desc-v {
      font-size: 1em;
   }

   .specs-row-v {
      border: 0;      
   }


</style>
   </head>
   <body class="is-preload">

      <!-- Wrapper -->
         <div id="wrapper">

            <!-- Main -->
               <div id="main">
                  <div class="inner">

                        <section>
                           <div class="row">
                              <div class="col-4 col-12-medium">
                                 <span class="image"><img class="specs-image-logo" style="max-height: 70px" src="http://miniselection.kehems.in/public/img/kehems-logo.gif" alt="Kehems Technologies"></span>
                              </div>

                              <div class="col-8 col-12-medium align-right">
                                 <h2 class="top_heading" style="color: #E32028;">{{ $model_details->model_name }}</h2>
                              </div>
                              
                           </div>
                        </section>
                        
                        <!--<header id="header"></header>-->

                        <div class="row">
                           &nbsp;
                        </div>

                           <!-- Contact person -->
                           <div class="row org-name">
                              <div class="col-8 col-8-medium"><strong>Client:</strong> {{ $client_name }}</div> 
                              <div class="col-4 col-4-medium" style="text-align: right;"><strong>Sales person:</strong> {{ $employee_name }}</div> 
                           </div>
                           <div class="row org-name">
                              <div class="col-10 col-12-medium"><strong>Consultant:</strong> {{ $consultant_name }}</div> 
                              <div class="col-2 col-12-medium" style="text-align: right;"></div> 
                           </div>
                           <div class="row org-name">
                              <div class="col-10 col-12-medium"><strong>Contractor:</strong> {{ $contractor_name }}</div> 
                              <div class="col-2 col-12-medium" style="text-align: right;"></div> 
                           </div>
                           <!-- Contact person ends -->
                           
                        <div class="row">
                           &nbsp;
                        </div>
                        
                        <!-- Range details -->
                        <section>
                           <div class="row">
                              <div class="col-7 col-12-medium prod-desc-v" style="text-align: left">
                              {{ $model_description }}
                                 <br><br><b>Dimensions:</b> {{ $model_details->dim_length }}mm x {{ $model_details->dim_width }}mm x {{ $model_details->dim_height }}mm | <b>Empty Weight:</b> {{ $model_details->dim_empty_weight }} Kg | <b>Operating Weight:</b> {{ $model_details->dim_operating_weight }} Kg | <b>Connection size:</b> {{ $model_details->dim_connection_size_condenser }} (Condenser), {{ $model_details->dim_connection_size_evaporator }} (Evaporator)
                              </div>
                              <div class="col-5 col-12-medium align-center">
                                 <span class="image"><img style="max-height: 150px;" class="specs-image-range" src="http://miniselection.kehems.in/public/img/ww_model/{{ $image_url }}" alt="Kehems Technologies"></span>
                              </div>
                              
                           </div>
                        </section>
                        <!-- Range details ends -->

                        <!--<section class="specs_section">-->
                        <!-- <section> -->


                           <!-- 
                           <div class="row">
                              &nbsp;
                           </div>
                           -->
                                                      
                           <div class="row">
                              
                              
                              <div class="col-6 col-12-medium">

                              <!-- #SECTION_GEN_SECTION# -->
                                 <div class="row specs-header-v">
                                    <div class="col-12 col-12-medium"><h4>GENERAL</h4></div>
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Heating Capacity:</b>&nbsp;{{ $model_details->gen_heating_capacity }} KW
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Cooling Capacity:</b>&nbsp;{{ $model_details->gen_cooling_capacity }} KW
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Compressor Absorbed Power:</b>&nbsp;{{ $model_details->gen_compressor_absorbed_power }} KW
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Compressor COP:</b>&nbsp;{{ round($model_details->gen_heating_capacity/$model_details->gen_compressor_absorbed_power, 2) }}
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Hot water Quantity:</b>&nbsp;{{ $model_details->gen_hot_water_quantity }} litre/hr
                                 </div>
                                 <!--
                                 <div class="row specs-row-v">
                                    <b>Cold water inlet temperature:</b>&nbsp;{{ $model_details->gen_cold_water_inlet_temperature }} C
                                 </div>
                                 -->
                                 <div class="row specs-row-v">
                                    <b>Hot water outlet temperature:</b>&nbsp;{{ $model_details->gen_hot_water_outlet_temperature }} &deg;C
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Chilled water outlet temperature:</b>&nbsp;{{ $model_details->gen_chilled_water_outlet_temperature }} &deg;C
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Refrigerant:</b>&nbsp;{{ $model_details->gen_refrigerant }}
                                 </div>

                                 <div class="row">
                                    &nbsp;
                                 </div>
                              </div>
                              
                              <!-- #SECTION_W_C_SECTION# ENDS -->
                              
                              <!-- <div class="col-1 col-12-medium"> </div> -->


                              <!-- #SECTION_W_C_SECTION# -->
                              <div class="col-6 col-12-medium">
                              
                                 <div class="row specs-header-v">
                                    <div class="col-12 col-12-medium"><h4>IN-BUILT HOT WATER PUMP, HYDRAULIC SYSTEM & CIP SYSTEM</h4></div>
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Hot water pump Flow:</b>&nbsp;{{ $model_details->hot_hot_water_pump_flow }} m³/h
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Hot water pump Head:</b>&nbsp;{{ $model_details->hot_hot_water_pump_head }} m
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Motor:</b>&nbsp;{{ $model_details->hot_motor }} KW
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Max Working pressure/Test pressure:</b>&nbsp;{{ $model_details->hot_max_working_pressure }}
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Piping & Valves:</b>&nbsp;{{ $model_details->hot_piping_and_valves }}
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>CIP System:</b>&nbsp;{{ $model_details->hot_cip_system }}
                                 </div>

                              </div>
                              <!-- #SECTION_W_C_SECTION# ENDS -->

                              <!-- <div class="col-1 col-12-medium"> </div> -->
                                 
                           </div>
                           
                           
                           <div class="row">


                              <!--  -->
                              <div class="col-6 col-12-medium">
                                 <div class="row specs-header-v">
                                    <div class="col-12 col-12-medium"><h4>COMPRESSOR</h4></div>
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Type:</b>&nbsp;{{ $model_details->com_type }}
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Quantity:</b>&nbsp;{{ $model_details->com_quantity }}
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Speed:</b>&nbsp;{{ $model_details->com_speed }} Rpm
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Capacity control:</b>&nbsp;{{ $model_details->com_capacity_control }}
                                  </div>

                                 <div class="row">
                                    &nbsp;
                                 </div>

                                 <div class="row specs-header-v">
                                    <div class="col-12 col-12-medium"><h4>CONDENSER HEAT EXCHANGER</h4></div>
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Type:</b>&nbsp;{{ $model_details->con_type }}
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Number:</b>&nbsp;{{ $model_details->con_number }}
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Nominal Flow:</b>&nbsp;{{ $model_details->con_nominal_flow }} m³/h
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Water pressure Drop:</b>&nbsp;{{ $model_details->con_water_pressure_drop }} M WG
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Max Working pressure/Test pressure (Water):</b>&nbsp;{{ $model_details->con_max_working_pressure_water }}
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Max Working pressure/Test pressure (Refrigerant):</b>&nbsp;{{ $model_details->con_max_working_pressure_refrigerant }}
                                  </div>

                                 <div class="row">
                                    &nbsp;
                                 </div>
                              </div>
                              <!--  -->

                              <!-- <div class="col-1 col-12-medium"> </div> -->

                              <!--  -->
                              <div class="col-6 col-12-medium">
                                 <div class="row specs-header-v">
                                    <div class="col-7 col-7-medium"><h4>IN-BUILT CHILLED WATER PUMP, HYDRAULIC SYSTEM & CIP SYSTEM</h4></div>
                                    <div class="col-5 col-5-medium"></div>
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Chilled water pump Flow:</b>&nbsp;{{ $model_details->chi_chilled_water_pump_flow }} m³/h
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Chilled water pump Head:</b>&nbsp;{{ $model_details->chi_chilled_water_pump_head }} m
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Motor:</b>&nbsp;{{ $model_details->chi_motor }} KW
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Max Working pressure/Test pressure:</b>&nbsp;{{ $model_details->chi_max_working_pressure }}
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Piping & Valves:</b>&nbsp;{{ $model_details->chi_piping_and_valves }}
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Closed type Expansion tank Capacity:</b>&nbsp;{{ $model_details->chi_closed_type_expansion_tank_capacity_ltr }} Litre
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>CIP System:</b>&nbsp;{{ $model_details->chi_cip_system }}
                                  </div>
                                 
                              </div>
                              <!-- #SECTION_H_P_SECTION# ENDS -->

                              <!-- <div class="col-1 col-12-medium"> </div> -->

                           </div>
                                                      
                           <div class="row">

                              
                              <div class="col-6 col-12-medium">

                                 <div class="row">
                                    <div class="col-10 col-10-medium specs-header-v"><h4>EVAPORATOR HEAT EXCHANGER</h4></div>
                                    <div class="col-2 col-2-medium"></div>
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Type:</b>&nbsp;{{ $model_details->eva_type }}
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Number:</b>&nbsp;{{ $model_details->eva_number }}
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Nominal Flow Flow:</b>&nbsp;{{ $model_details->eva_nominal_flow }} m³/h
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Water pressure Drop:</b>&nbsp;{{ $model_details->eva_water_pressure_drop }} M WG
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Max Working pressure/Test pressure (Water):</b>&nbsp;{{ $model_details->eva_max_working_pressure_water }}
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Max Working pressure/Test pressure (Refrigerant):</b>&nbsp;{{ $model_details->eva_max_working_pressure_refrigerant }}
                                  </div>

                                 <div class="row">
                                    &nbsp;
                                 </div>
                                 
                              </div>


                              <div class="col-6 col-12-medium">                                 
                              <!-- Electrical -->
                                 <div class="row specs-header-v">
                                    <div class="col-12 col-12-medium">
                                       <h4>ELECTRICAL</h4>
                                    </div>
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Supply power:</b>&nbsp;{{ $model_details->ele_supply_power }}
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Nominal compressor current:</b>&nbsp;{{ $model_details->ele_nominal_compressor_current }}
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Compressor Starting Current-Soft starter:</b>&nbsp;{{ $model_details->ele_compressor_starting_current }}
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Electrical Installation:</b>&nbsp;{{ $model_details->ele_electrical_installation }} KW
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Incoming selection current:</b>&nbsp;{{ $model_details->ele_incoming_selection_current }}
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Recommended Minimum Cable size:</b>&nbsp;{{ $model_details->ele_recommended_minimum_cable_size }}
                                 </div>
                                 <div class="row specs-row-v">
                                    <b>Recommended Cable Type:</b>&nbsp;{{ $model_details->ele_recommended_cable_type }}
                                 </div>
                                 
                                 <div class="row">
                                    &nbsp;
                                 </div>
                              <!-- Electrical ends -->

                              </div>

                              <!-- <div class="col-1 col-12-medium"> </div> -->

                           </div>
                           
             
                       <!-- </section> -->

                        <section>
                           <div class="row">
                              <div class="col-2 col-2-medium"></div> 
                              <div class="col-10 col-10-medium" style="text-align: right;">{{ $date_created }} | Kehems Technologies Pvt. Ltd.</div> 
                           </div>
                        </section>
                        
                        <div class="row">
                           &nbsp;
                        </div>
                        
                  </div>
               </div>

         </div>


   </body>
</html>


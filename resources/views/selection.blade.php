@php $page = 'selection'; @endphp
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img//apple-icon.png">
  <link rel="icon" type="image/png" href="{{ asset('public/img//favicon.png') }}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Kehems | Selection
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('public/css/paper-dashboard.css?v=2.1.1') }}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{ asset('public/demo/demo.css') }}" rel="stylesheet" />
</head>

<body class="">
  <div class="wrapper ">
    @include('layouts.sidebar');
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-minimize">
              <button id="minimizeSidebar" class="btn btn-icon btn-round">
                <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
                <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
              </button>
            </div>
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="javascript:;">Selection</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <!--<div class="collapse navbar-collapse justify-content-end" id="navigation">
            <form>
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <i class="nc-icon nc-zoom-split"></i>
                  </div>
                </div>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link btn-magnify" href="javascript:;">
                  <i class="nc-icon nc-layout-11"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Stats</span>
                  </p>
                </a>
              </li>
              <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="nc-icon nc-bell-55"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <a class="dropdown-item" href="#">Something else here</a>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link btn-rotate" href="javascript:;">
                  <i class="nc-icon nc-settings-gear-65"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Account</span>
                  </p>
                </a>
              </li>
            </ul>
          </div>-->
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          
          <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                <h5 class="title">Selection Criteria</h5>
              </div>
              <div class="card-body">
                <form>
                                     
                  <label>Application Type</label>
                  <div class="row">
                    <div class="col-md-4 pr-1">
                      <div class="form-group">
                        <div class="form-check-radio">
                         <label class="form-check-label">
                          <input class="form-check-input" type="radio" name="exampleRadioz" id="exampleRadios11" value="option1">
                          Air to Water
                          <span class="form-check-sign"></span>
                         </label>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4 pr-1">
                      <div class="form-group">
                        <div class="form-check-radio">
                         <label class="form-check-label">
                          <input class="form-check-input" type="radio" name="exampleRadioz" id="exampleRadios11" value="option1">
                          Water to Water
                          <span class="form-check-sign"></span>
                         </label>
                        </div>
                      </div>
                    </div>

                  </div>


                  <div class="row">
                    <div class="col-md-3 pr-1">
                      <div class="form-group">
                        <label>Ambient Temp.</label>
                        <input type="text" class="form-control" placeholder="Company" value="25">
                      </div>
                    </div>
                    <div class="col-md-3 px-1">
                      <div class="form-group">
                        <label>Outlet Temp.</label>
                        <input type="text" class="form-control" placeholder="Username" value="55">
                      </div>
                    </div>
                    <div class="col-md-3 pl-1">
                      <div class="form-group">
                        <label>Ltrs/day - From</label>
                        <input type="text" class="form-control" placeholder="100">
                      </div>
                    </div>

                    <div class="col-md-3 pl-1">
                      <div class="form-group">
                        <label>To</label>
                        <input type="text" class="form-control" placeholder="100">
                      </div>
                    </div>

                  </div>
                  
                  <div class="row">
                    <div class="col-md-3 pr-1">
                      <div class="form-group">
                        <label>Pump Head</label>
                           <select class="form-control" name="cars" id="cars">
                             <option value="volvo"><=</option>
                             <option value="saab">>=</option>
                           </select>                        
                      </div>
                    </div>

                    <div class="col-md-3 pr-1">
                      <div class="form-group">
                        <label>&nbsp;</label>
                        <input type="text" class="form-control" placeholder="Company" value="15">
                      </div>
                    </div>

                    <div class="col-md-6 pl-1">
                      <div class="form-group">
                        <label>Refrigerant</label>
                           <select class="form-control" name="cars" id="cars">
                             <option value="volvo">R134A</option>
                             <option value="saab">R410 A</option>
                           </select>                        
                      </div>
                    </div>
                  </div>
                  
                  <button class="btn btn-primary btn-sm">Search</button>

                </form>
              </div>
            </div>
          </div>
          

           <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                <h5 class="title">Project Details</h5>
              </div>
              <div class="card-body">
                <form>
                
                  <div class="row">
                    <div class="col-md-4 pr-1">
                      <div class="form-group">
                        <label>Name of the Project</label>
                        <input type="text" class="form-control" placeholder="" value="">
                      </div>
                    </div>
                    <div class="col-md-4 px-1">
                      <div class="form-group">
                        <label>Location</label>
                        <input type="text" class="form-control" placeholder="" value="">
                      </div>
                    </div>
                    <div class="col-md-4 px-1">
                      <div class="form-group">
                        <label>Client</label>
                           <select class="form-control" name="cars" id="cars">
                             <option value="volvo">Taj hotel, Mumbai</option>
                             <option value="saab">Jupiter hospital, Indore</option>
                           </select>
                      </div>
                    </div>
                  </div>
                  

                  <div class="row">
                    <div class="col-md-4 pr-1">
                      <div class="form-group">
                        <label>Nature of Client</label>
                           <select class="form-control" name="cars" id="cars">
                             <option value="volvo">Direct Client</option>
                             <option value="saab">Chanel Partner</option>
                           </select>
                      </div>
                    </div>
                    <div class="col-md-4 px-1">
                      <div class="form-group">
                        <label>Consultant</label>
                           <select class="form-control" name="cars" id="cars">
                             <option value="">Select</option>
                           </select>
                      </div>
                    </div>
                    <div class="col-md-4 px-1">
                      <div class="form-group">
                        <label>Contractor</label>
                           <select class="form-control" name="cars" id="cars">
                             <option value="">Select</option>
                           </select>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-8 pr-1">
                      <div class="form-group">
                        <label>Sales person</label>
                           <select class="form-control" name="cars" id="cars">
                             <option value="">Select</option>
                           </select>
                      </div>
                    </div>
                    <div class="col-md-4 px-1">
                      <div class="form-group">
                        <label>Region</label>
                           <select class="form-control" name="cars" id="cars">
                             <option value="West">West</option>
                             <option value="Central">Central</option>
                             <option value="East">East</option>
                             <option value="North">North</option>
                             <option value="South">South</option>
                           </select>
                      </div>
                    </div>
                  </div>

                  
                </form>
              </div>
            </div>
          </div>
          
                    
        </div>
        
<div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Models</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class="text-primary">
                      <tr><th>
                        Name
                      </th>
                      <th>
                        Heating cap.
                      </th>
                      <th>
                        Ab. power
                      </th>
                      <th>
                        Cooling cap.
                      </th>
                      <th>
                        Pump head
                      </th>
                      <th>
                        Refrigerant
                      </th>
                      <th>
                        Litre/day
                      </th>
                      <th>
                        
                      </th>
                    </tr></thead>
                    <tbody>
                      <tr>
                        <td>KRISTHERM MINI II 7KW HP</td>
                        <td>7.1</td>
                        <td>2.1</td>
                        <td> </td>
                        <td>13.00</td>
                        <td>R134A</td>
                        <td>203.53</td>
                        <td class="text-right">
                          <button type="button" rel="tooltip" class="btn btn-info btn-icon btn-sm " data-original-title="" title="View">
                            <i class="fa fa-file"></i>
                          </button>
                          <!--<button type="button" rel="tooltip" class="btn btn-success btn-icon btn-sm " data-original-title="" title="Send Quote">
                            <i class="fa fa-paper-plane"></i>
                          </button>-->
                        </td>
                      </tr>

                      <tr>
                        <td>KRISTHERM MINI II 7KW HP</td>
                        <td>7.1</td>
                        <td>2.1</td>
                        <td> </td>
                        <td>13.00</td>
                        <td>R134A</td>
                        <td>203.53</td>
                        <td class="text-right">
                          <button type="button" rel="tooltip" class="btn btn-info btn-icon btn-sm " data-original-title="" title="View">
                            <i class="fa fa-file"></i>
                          </button>
                          <!--<button type="button" rel="tooltip" class="btn btn-success btn-icon btn-sm " data-original-title="" title="Send Quote">
                            <i class="fa fa-paper-plane"></i>
                          </button>-->
                        </td>
                      </tr>

                      <tr>
                        <td>KRISTHERM MINI II 7KW HP</td>
                        <td>7.1</td>
                        <td>2.1</td>
                        <td> </td>
                        <td>13.00</td>
                        <td>R134A</td>
                        <td>203.53</td>
                        <td class="text-right">
                          <button type="button" rel="tooltip" class="btn btn-info btn-icon btn-sm " data-original-title="" title="View">
                            <i class="fa fa-file"></i>
                          </button>
                          <!--<button type="button" rel="tooltip" class="btn btn-success btn-icon btn-sm " data-original-title="" title="Send Quote">
                            <i class="fa fa-paper-plane"></i>
                          </button>-->
                        </td>
                      </tr>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
      <footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
          <div class="row">
            <!--<nav class="footer-nav">
              <ul>
                <li><a href="https://www.creative-tim.com" target="_blank">Creative Tim</a></li>
                <li><a href="https://www.creative-tim.com/blog" target="_blank">Blog</a></li>
                <li><a href="https://www.creative-tim.com/license" target="_blank">Licenses</a></li>
              </ul>
            </nav>-->
            <div class="credits ml-auto">
              <span class="copyright">
                © <script>
                  document.write(new Date().getFullYear())
                </script>, kehems.com
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="{{ asset('public/js/core/jquery.min.js') }}"></script>
  <script src="{{ asset('public/js/core/popper.min.js') }}"></script>
  <script src="{{ asset('public/js/core/bootstrap.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/moment.min.js') }}"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="{{ asset('public/js/plugins/bootstrap-switch.js') }}"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="{{ asset('public/js/plugins/sweetalert2.min.js') }}"></script>
  <!-- Forms Validations Plugin -->
  <script src="{{ asset('public/js/plugins/jquery.validate.min.js') }}"></script>
  <!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="{{ asset('public/js/plugins/jquery.bootstrap-wizard.js') }}"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="{{ asset('public/js/plugins/bootstrap-selectpicker.js') }}"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="{{ asset('public/js/plugins/bootstrap-datetimepicker.js') }}"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
  <script src="{{ asset('public/js/plugins/jquery.dataTables.min.js') }}"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="{{ asset('public/js/plugins/bootstrap-tagsinput.js') }}"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="{{ asset('public/js/plugins/jasny-bootstrap.min.js') }}"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="{{ asset('public/js/plugins/fullcalendar/fullcalendar.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/fullcalendar/daygrid.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/fullcalendar/timegrid.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/fullcalendar/interaction.min.js') }}"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="{{ asset('public/js/plugins/jquery-jvectormap.js') }}"></script>
  <!--  Plugin for the Bootstrap Table -->
  <script src="{{ asset('public/js/plugins/nouislider.min.js') }}"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="{{ asset('public/js/plugins/chartjs.min.js') }}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{ asset('public/js/plugins/bootstrap-notify.js') }}"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('public/js/paper-dashboard.min.js?v=2.1.1') }}" type="text/javascript"></script><!-- Paper Dashboard DEMO methods, don't include it in your project! -->
  <script src="{{ asset('public/demo/demo.js') }}"></script>
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      demo.initDashboardPageCharts();


      demo.initVectorMap();

    });
  </script>
</body>

</html>
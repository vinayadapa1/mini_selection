@php $page = 'sales_employee'; @endphp
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img//apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img//favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Kehems | Add/Update Sales Employee
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('public/css/paper-dashboard.css?v=2.1.1') }}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{ asset('public/demo/demo.css') }}" rel="stylesheet" />
</head>

<body class="">
  <div class="wrapper ">
  @include('layouts.sidebar');
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-minimize">
              <button id="minimizeSidebar" class="btn btn-icon btn-round">
                <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
                <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
              </button>
            </div>
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="javascript:;">Master</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
            <div class="col-md-12">
            <form id="" action="{{ route('add-sales-employee') }}" method="POST">
			  @csrf
              <div class="card ">
                <div class="card-header ">
                  <h4 class="card-title">Add/Update Sales Employee</h4>
                </div>
                <div class="card-body ">
				  <div class="row">
				  <input class="form-control" name="employee_id" type="hidden" value=""/>
                  <div class="form-group has-label col-md-6">
                    <label>
                      First Name
                      *
                    </label>
                    <input class="form-control" name="first_name" type="text" required="true" value="" />
                  </div>
                  <div class="form-group has-label col-md-6">
                    <label>
                      Last Name
                      *
                    </label>
                    <input class="form-control" name="last_name" id="last_name" type="text" required="true" />
                  </div>
				  </div>
				  <div class="row">
                  <div class="form-group has-label col-md-6">
                    <label>
                      Contact No.
                      *
                    </label>
                    <input class="form-control" name="contact_no" type="text" required="true" />
                  </div>
                  <div class="form-group has-label col-md-6">
                    <label>
                      Extention
                      *
                    </label>
                    <input class="form-control" name="extention" type="text" required="true" />
                  </div>
				  </div>
				  <div class="row">
				    <div class="form-group has-label col-md-6">
                    <label>
                      Email Id
                      *
                    </label>
                    <input class="form-control" name="email_id" id="email_id" type="email" required="true" />
                  </div>
				  <div class="form-group has-label col-md-6">
                    <label>
                      Region
                      *
                    </label>
                    <select class="form-control" name="region" required="true" >
					    <option value="Central">Central</option>
                        <option value="East">East</option>
                        <option value="North">North</option>
                        <option value="West">West</option>
                        <option value="South">South</option>   						
					</select>
                  </div>
				  </div>
				  <div class="row">
				  <div class="form-group has-label col-md-6">
                    <label>
                      Department
                      *
                    </label>
                    <select class="form-control" name="department" required="true" >
					    <option value="Sales">Sales</option>
                        <option value="Marketing">Marketing</option>
                        <option value="Admin">Admin</option>   						
					</select>
                  </div>
				  </div>
				  <div class="row">
                  <div class="form-group has-label col-md-6">
                    <label>
                      City
                      *
                    </label>
                    <input class="form-control" name="city" type="text" required="true" />
                  </div>
                  <div class="form-group has-label col-md-6">
                    <label>
                      State
                      *
                    </label>
                    <input class="form-control" name="state" id="state" type="text" required="true" />
                  </div>
				  </div>
				  <div class="row">
                  <div class="form-group has-label col-md-6">
                    <label>
                      Country
                      *
                    </label>
                    <input class="form-control" name="country" type="text" required="true" />
                  </div>
                  <div class="form-group has-label col-md-6">
                    <label>
                      Pincode
                      *
                    </label>
                    <input class="form-control" name="pincode" id="pincode" type="text" required="true" />
                  </div>
				  </div>
				  <div class="row">
				    <div class="form-group has-label col-md-6">
					    <label>
                      Address
                      *
                    </label>
					   <textarea class="form-control" name="address" id="address" rows="5" required="true"></textarea>
					</div>
				  </div>
                </div>
                <div class="card-footer text-right">
                    <button type="submit" class="btn btn-primary" name="add_update_employee" value="add_update">Submit</button>
                </div>
              </div>
            </form>
          </div> 
        </div> <!-- end row -->
      </div>
      <footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
          <div class="row">
            <!--<nav class="footer-nav">
              <ul>
                <li><a href="https://www.creative-tim.com" target="_blank">Creative Tim</a></li>
                <li><a href="https://www.creative-tim.com/blog" target="_blank">Blog</a></li>
                <li><a href="https://www.creative-tim.com/license" target="_blank">Licenses</a></li>
              </ul>
            </nav>-->
            <div class="credits ml-auto">
              <span class="copyright">
                © <script>
                  document.write(new Date().getFullYear())
                </script>, kehems.com
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="{{ asset('public/js/core/jquery.min.js') }}"></script>
  <script src="{{ asset('public/js/core/popper.min.js') }}"></script>
  <script src="{{ asset('public/js/core/bootstrap.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/moment.min.js') }}"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="{{ asset('public/js/plugins/bootstrap-switch.js') }}"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="{{ asset('public/js/plugins/sweetalert2.min.js') }}"></script>
  <!-- Forms Validations Plugin -->
  <script src="{{ asset('public/js/plugins/jquery.validate.min.js') }}"></script>
  <!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="{{ asset('public/js/plugins/jquery.bootstrap-wizard.js') }}"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="{{ asset('public/js/plugins/bootstrap-selectpicker.js') }}"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="{{ asset('public/js/plugins/bootstrap-datetimepicker.js') }}"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
  <script src="{{ asset('public/js/plugins/jquery.dataTables.min.js') }}"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="{{ asset('public/js/plugins/bootstrap-tagsinput.js') }}"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="{{ asset('public/js/plugins/jasny-bootstrap.min.js') }}"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="{{ asset('public/js/plugins/fullcalendar/fullcalendar.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/fullcalendar/daygrid.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/fullcalendar/timegrid.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/fullcalendar/interaction.min.js') }}"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="{{ asset('public/js/plugins/jquery-jvectormap.js') }}"></script>
  <!--  Plugin for the Bootstrap Table -->
  <script src="{{ asset('public/js/plugins/nouislider.min.js') }}"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="{{ asset('public/js/plugins/chartjs.min.js') }}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{ asset('public/js/plugins/bootstrap-notify.js') }}"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('public/js/paper-dashboard.min.js?v=2.1.1') }}" type="text/javascript"></script><!-- Paper Dashboard DEMO methods, don't include it in your project! -->
  <script src="{{ asset('public/demo/demo.js') }}"></script>
  <script>
    $(document).ready(function() {
      //
    });
  </script>
</body>

</html>
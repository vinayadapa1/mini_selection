<div class="sidebar" data-color="white" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color=" default | primary | info | success | warning | danger |"
    -->
      <div class="logo">
        <a href="#" class="simple-text logo-mini" style="display:none">
          <div class="logo-image-small">
            <img src="{{ asset('public/img/logo-small.png') }}">
          </div>
          <!-- <p>CT</p> -->
        </a>
        <a href="#" class="simple-text">
           <div class="logo-image-big">
            <img src="{{ asset('public/img/kehems-logo.gif') }}">
          </div> 
        </a>
      </div>
      <div class="sidebar-wrapper">
        <div class="user">
          <div class="photo">
            <img src="{{ asset('public/img/default-avatar.png') }}" />
          </div>
          <div class="info">
            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
              <span>
                {{ auth()->user()->name }}
                <!--<b class="caret"></b>-->
              </span>
            </a>
            <div class="clearfix"></div>
            <!--<div class="collapse" id="collapseExample">
              <ul class="nav">
                <li>
                  <a href="#">
                    <span class="sidebar-mini-icon">MP</span>
                    <span class="sidebar-normal">My Profile</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="sidebar-mini-icon">EP</span>
                    <span class="sidebar-normal">Edit Profile</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="sidebar-mini-icon">S</span>
                    <span class="sidebar-normal">Settings</span>
                  </a>
                </li>
				<li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <span class="sidebar-mini-icon">L</span>
                    <span class="sidebar-normal">Logout</span>
                </a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
                </li>
              </ul>
            </div>-->
          </div>
        </div>
        <ul class="nav">
          <!--<li class="active">
            <a href="../examples/dashboard.html">
              <i class="nc-icon nc-bank"></i>
              <p>Dashboard</p>
            </a>
          </li>-->
		  <li class="{{ ($page == 'selection') ? 'active' : ''; }}">
            <a href="{{ url('selection') }}">
              <i class="nc-icon nc-bank"></i>
              <p>Selection</p>
            </a>
          </li>
		  <!--<li class="">
            <a href="#">
              <i class="nc-icon nc-bank"></i>
              <p>Prev. Quotes</p>
            </a>
          </li>-->
          <li class="{{ ($page == 'sales_employee' || $page == 'client' || $page == 'consultant' || $page == 'contractor') ? 'active' : ''; }}">
            <a data-toggle="collapse" href="#pagesExamples">
              <i class="nc-icon nc-book-bookmark"></i>
              <p>
                Masters <b class="caret"></b>
              </p>
            </a>
            <div class="collapse " id="pagesExamples">
              <ul class="nav">
                <li>
                  <a href="{{ url('sales-employee') }}">
                    <span class="sidebar-mini-icon">SE</span>
                    <span class="sidebar-normal"> Sales Employee </span>
                  </a>
                </li>
                <li>
                  <a href="{{ url('clients') }}">
                    <span class="sidebar-mini-icon">C</span>
                    <span class="sidebar-normal"> Client </span>
                  </a>
                </li>
                <li>
                  <a href="{{ url('consultants') }}">
                    <span class="sidebar-mini-icon">C</span>
                    <span class="sidebar-normal"> Consultant </span>
                  </a>
                </li>
                <li>
                  <a href="{{ url('contractors') }}">
                    <span class="sidebar-mini-icon">C</span>
                    <span class="sidebar-normal"> Contractor </span>
                  </a>
                </li>
                <!--<li>
                  <a href="../examples/pages/user.html">
                    <span class="sidebar-mini-icon">UP</span>
                    <span class="sidebar-normal"> User Profile </span>
                  </a>
                </li>-->
              </ul>
            </div>
          </li>
		  @if(auth()->user()->type == 'admin')
		  <li class="{{ ($page == 'users') ? 'active' : ''; }}">
            <a href="{{ url('users') }}">
              <i class="nc-icon nc-bank"></i>
              <p>User Management</p>
            </a>
          </li>
		  @endif
		  <li class="">
            <a href="{{ url('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
              <i class="nc-icon nc-bank"></i>
              <p>Logout</p>
            </a>
			<form id="logout-form" action="{{ url('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
          </li>
        </ul>
      </div>
    </div>
@php $page = 'sales_employee'; @endphp
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img//apple-icon.png">
  <link rel="icon" type="image/png" href="{{ asset('public/img//favicon.png') }}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Kehems | Edit Consultant
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('public/css/paper-dashboard.css?v=2.1.1') }}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{ asset('public/demo/demo.css') }}" rel="stylesheet" />

  <style>
    .error {
      color: #dc3545
    }
  </style>
</head>

<body class="">
  <div class="wrapper ">
  @include('layouts.sidebar')
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-minimize">
              <button id="minimizeSidebar" class="btn btn-icon btn-round">
                <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
                <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
              </button>
            </div>
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="javascript:;">Master</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- if there are creation errors, they will show here -->

            <form id="" action="{{ url('consultants/'. $consultant->id) }}" method="POST">
			  @csrf
              @method('put')
              <div class="card ">
                <div class="card-header ">
                  <h4 class="card-title">Edit Consultant</h4>
                </div>
                <div class="card-body ">
				  <div class="row">
                  <div class="form-group has-label col-md-6">
                    <label>
                      First Name
                      *
                    </label>
                    <input class="form-control" name="first_name" type="text" value="{{ $consultant->first_name }}" />
                    @if($errors->has('first_name'))
                      <div class="error">{{ $errors->first('first_name') }}</div>
                    @endif
                  </div>
                  <div class="form-group has-label col-md-6">
                    <label>
                      Last Name
                      *
                    </label>
                    <input class="form-control" name="last_name" type="text" value="{{ $consultant->last_name }}" />
                    @if($errors->has('first_name'))
                      <div class="error">{{ $errors->first('last_name') }}</div>
                    @endif
                  </div>
				  </div>
				  <div class="row">
                  <div class="form-group has-label col-md-6">
                    <label>
                      Contact No.
                      *
                    </label>
                    <input class="form-control" name="contact_no" maxlength=10 type="text" value="{{ $consultant->contact_no }}"/>
                    @if($errors->has('contact_no'))
                      <div class="error">{{ $errors->first('contact_no') }}</div>
                    @endif
                  </div>
                  <div class="form-group has-label col-md-6">
                    <label>
                      Extention
                      
                    </label>
                    <input class="form-control" name="extention" type="text" value="{{ $consultant->extention }}" />
                  </div>
				  </div>
				  <div class="row">
				    <div class="form-group has-label col-md-6">
                    <label>
                      Email Id
                      
                    </label>
                    <input class="form-control" name="email_id" type="email" value="{{ $consultant->email_id }}" />
                  </div>
				  <div class="form-group has-label col-md-6">
                    <label>
                      Region
                      
                    </label>
                    <select class="form-control" name="region">
                        <option value="">Select</option>
					    <option value="Central" {{ ($consultant->region == 'Central') ? 'selected' : '' }}>Central</option>
                        <option value="East" {{ ($consultant->region == 'East') ? 'selected' : '' }}>East</option>
                        <option value="North" {{ ($consultant->region == 'North') ? 'selected' : '' }}>North</option>
                        <option value="West" {{ ($consultant->region == 'West') ? 'selected' : '' }}>West</option>
                        <option value="South" {{ ($consultant->region == 'South') ? 'selected' : '' }}>South</option>   						
					</select>
                  </div>
				  </div>
				  <!--<div class="row">
				  <div class="form-group has-label col-md-6">
                    <label>
                      Department
                      
                    </label>
                    <select class="form-control" name="department">
                        <option value="">Select</option>
                        <option value="Sales" {{ ($consultant->department == 'Sales') ? 'selected' : '' }}>Sales</option>
                        <option value="Marketing" {{ ($consultant->department == 'Marketing') ? 'selected' : '' }}>Marketing</option>
                        <option value="Admin" {{ ($consultant->department == 'Admin') ? 'selected' : '' }}>Admin</option>   						
					</select>
                  </div>
				  </div>-->
				  <div class="row">
                  <div class="form-group has-label col-md-6">
                    <label>
                      City
                      
                    </label>
                    <input class="form-control" name="city" type="text" value="{{ $consultant->city }}" />
                  </div>
                  <div class="form-group has-label col-md-6">
                    <label>
                      State
                      
                    </label>
                    <input class="form-control" name="state" type="text" value="{{ $consultant->state }}" />
                  </div>
				  </div>
				  <div class="row">
                  <div class="form-group has-label col-md-6">
                    <label>
                      Country
                      
                    </label>
                    <input class="form-control" name="country" type="text" value="{{ $consultant->country }}" />
                  </div>
                  <div class="form-group has-label col-md-6">
                    <label>
                      Pincode
                      
                    </label>
                    <input class="form-control" name="pincode" maxlength=6 type="text" value="{{ $consultant->pincode }}" />
                  </div>
				  </div>
				  <div class="row">
				    <div class="form-group has-label col-md-6">
					    <label>
                      Address
                      
                    </label>
					   <textarea class="form-control" name="address" rows="5">{{ $consultant->address }}</textarea>
					</div>
				  </div>
                </div>
                <div class="card-footer text-right">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </div>
            </form>
          </div> 
        </div> <!-- end row -->
      </div>
      <footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
          <div class="row">
            <!--<nav class="footer-nav">
              <ul>
                <li><a href="https://www.creative-tim.com" target="_blank">Creative Tim</a></li>
                <li><a href="https://www.creative-tim.com/blog" target="_blank">Blog</a></li>
                <li><a href="https://www.creative-tim.com/license" target="_blank">Licenses</a></li>
              </ul>
            </nav>-->
            <div class="credits ml-auto">
              <span class="copyright">
                © <script>
                  document.write(new Date().getFullYear())
                </script>, kehems.com
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="{{ asset('public/js/core/jquery.min.js') }}"></script>
  <script src="{{ asset('public/js/core/popper.min.js') }}"></script>
  <script src="{{ asset('public/js/core/bootstrap.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/moment.min.js') }}"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="{{ asset('public/js/plugins/bootstrap-switch.js') }}"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="{{ asset('public/js/plugins/sweetalert2.min.js') }}"></script>
  <!-- Forms Validations Plugin -->
  <script src="{{ asset('public/js/plugins/jquery.validate.min.js') }}"></script>
  <!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="{{ asset('public/js/plugins/jquery.bootstrap-wizard.js') }}"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="{{ asset('public/js/plugins/bootstrap-selectpicker.js') }}"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="{{ asset('public/js/plugins/bootstrap-datetimepicker.js') }}"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
  <script src="{{ asset('public/js/plugins/jquery.dataTables.min.js') }}"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="{{ asset('public/js/plugins/bootstrap-tagsinput.js') }}"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="{{ asset('public/js/plugins/jasny-bootstrap.min.js') }}"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="{{ asset('public/js/plugins/fullcalendar/fullcalendar.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/fullcalendar/daygrid.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/fullcalendar/timegrid.min.js') }}"></script>
  <script src="{{ asset('public/js/plugins/fullcalendar/interaction.min.js') }}"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="{{ asset('public/js/plugins/jquery-jvectormap.js') }}"></script>
  <!--  Plugin for the Bootstrap Table -->
  <script src="{{ asset('public/js/plugins/nouislider.min.js') }}"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="{{ asset('public/js/plugins/chartjs.min.js') }}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{ asset('public/js/plugins/bootstrap-notify.js') }}"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('public/js/paper-dashboard.min.js?v=2.1.1') }}" type="text/javascript"></script><!-- Paper Dashboard DEMO methods, don't include it in your project! -->
  <script src="{{ asset('public/demo/demo.js') }}"></script>
  <script>
    $(document).ready(function() {
        $("input[name='contact_no'], input[name='extention'], input[name='pincode']").keypress(function(e) {  
            var arr = [];  
            var kk = e.which;  
     
            for (i = 48; i < 58; i++)  
            arr.push(i);  
     
            if (!(arr.indexOf(kk)>=0))  
                e.preventDefault();  
        });  
    });
  </script>
</body>

</html>
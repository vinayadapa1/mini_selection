<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class UserController extends Controller
{
    public function index(Request $request)
	{
		$user_details = DB::table('users')
		                      ->select('*')
							  ->get();
							  
		return view('users', compact('user_details'));		
	}
}

<?php

namespace App\Http\Controllers;

use App\Models\SalesEmployee;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;

class SalesEmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the employee
        $employee_details = SalesEmployee::all();

        // load the view and pass the employee
        return view('sales_employee.index')
            ->with('employee_details', $employee_details);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sales_employee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $validator = $request->validate([
            'first_name'       => 'required',
            'last_name'        => 'required',
            'contact_no'       => 'required|numeric|digits:10'
            /*'extention'        => 'required',
            'email_id'         => 'required',
            'region'           => 'required',
            'department'       => 'required',
            'city'             => 'required',
            'state'            => 'required',
            'country'          => 'required',
            'pincode'          => 'required',
            'address'          => 'required'*/
        ], [
            'first_name.required'       => 'please enter first name',
            'last_name.required'        => 'please enter last name',
            'contact_no.required'       => 'please enter contact no',
            'contact_no.digits'          => 'please enter valid contact no'
            /*'extention.required'        => 'please enter extention',
            'email_id.required'         => 'please enter email_id',
            'region.required'           => 'please enter region',
            'department.required'       => 'please enter department',
            'city.required'             => 'please enter city',
            'state.required'            => 'please enter state',
            'country.required'          => 'please enter country',
            'pincode.required'          => 'please enter pincode',
            'address.required'          => 'please enter address'*/
        ]);

        // process the login
        /*if ($validator->fails()) {
            return redirect('sales-employee/create')
                ->withErrors($validator)
                ->withInput();
        } else {*/
            // store
            $employee = new SalesEmployee;
            $employee->first_name           = $request->input('first_name');
            $employee->last_name            = $request->input('last_name');
            $employee->contact_no           = $request->input('contact_no');
			$employee->extention            = $request->input('extention');
			$employee->email_id             = $request->input('email_id');
			$employee->region               = $request->input('region');
			$employee->department           = $request->input('department');
			$employee->city                 = $request->input('city');
			$employee->state                = $request->input('state');
			$employee->country              = $request->input('country');
			$employee->pincode              = $request->input('pincode');
			$employee->address              = $request->input('address');
			$employee->status               = 'active';
			$employee->created_at           = date("Y-m-d h:i:s");
            $employee->updated_at           = date("Y-m-d h:i:s");
			$employee->created_by           = auth()->user()->id;

            $employee->save();

            // redirect
            Session::flash('message', 'Successfully created Employee!');
            return redirect('sales-employee');
        //}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SalesEmployee  $salesEmployee
     * @return \Illuminate\Http\Response
     */
    public function show(SalesEmployee $salesEmployee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SalesEmployee  $salesEmployee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the employee
        $employee= SalesEmployee::find($id);

        // show the edit form and pass the shark
        return view('sales_employee.edit')
            ->with('employee', $employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SalesEmployee  $salesEmployee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $validator = $request->validate([
            'first_name'       => 'required',
            'last_name'        => 'required',
            'contact_no'       => 'required|numeric|digits:10'
            /*'extention'        => 'required',
            'email_id'         => 'required',
            'region'           => 'required',
            'department'       => 'required',
            'city'             => 'required',
            'state'            => 'required',
            'country'          => 'required',
            'pincode'          => 'required',
            'address'          => 'required'*/
        ], [
            'first_name.required'       => 'please enter first name',
            'last_name.required'        => 'please enter last name',
            'contact_no.required'       => 'please enter contact no',
            'contact_no.digits'          => 'please enter valid contact no'
            /*'extention.required'        => 'please enter extention',
            'email_id.required'         => 'please enter email_id',
            'region.required'           => 'please enter region',
            'department.required'       => 'please enter department',
            'city.required'             => 'please enter city',
            'state.required'            => 'please enter state',
            'country.required'          => 'please enter country',
            'pincode.required'          => 'please enter pincode',
            'address.required'          => 'please enter address'*/
        ]);

        // process the login
        /*if ($validator->fails()) {
            return Redirect::to('sales-employee/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        } else {*/
            // store
            $employee = SalesEmployee::find($id);
            $employee->first_name           = $request->input('first_name');
            $employee->last_name            = $request->input('last_name');
            $employee->contact_no           = $request->input('contact_no');
			$employee->extention            = $request->input('extention');
			$employee->email_id             = $request->input('email_id');
			$employee->region               = $request->input('region');
			$employee->department           = $request->input('department');
			$employee->city                 = $request->input('city');
			$employee->state                = $request->input('state');
			$employee->country              = $request->input('country');
			$employee->pincode              = $request->input('pincode');
			$employee->address              = $request->input('address');
			//$employee->status               = 'active';
			//$employee->created_at           = date("Y-m-d h:i:s");
            $employee->updated_at           = date("Y-m-d h:i:s");
			//$employee->created_by           = auth()->user()->id;

            $employee->save();

            // redirect
            Session::flash('message', 'Successfully updated employee!');
            return redirect('sales-employee');
        //}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SalesEmployee  $salesEmployee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $employee = SalesEmployee::find($id);
        $employee->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the employee!');
        return redirect('sales-employee');
    }
}

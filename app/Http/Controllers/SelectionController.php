<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;

use App\Models\Client;
use App\Models\Consultant;
use App\Models\Contractor;
use App\Models\SalesEmployee;
use Illuminate\Http\Request;

use DB;

class SelectionController extends Controller
{
    public function index()
    {
        // get all the client
        $client_details = Client::all();
        
        // get all the consultant
        $consultant_details = Consultant::all();

        // get all the contractor
        $contractor_details = Contractor::all();

        // get all the sales employee
        $employee_details = SalesEmployee::all();

        // load the view and pass the contractor
        return view('selection.index')
            ->with(['contractor_details' => $contractor_details, 'consultant_details' => $consultant_details, 'employee_details' => $employee_details, 'client_details' => $client_details]);
    }

    public function generateReport(Request $request)
    {
        $ip_application_type = $request->input('ip_application_type');
        $ip_ambient_temperature = $request->input('ip_ambient_temperature');
        $ip_output_temperature = $request->input('ip_output_temperature');
        $ip_litres_per_day_range_from = $request->input('ip_litres_per_day_range_from');
        $ip_litres_per_day_range_to = $request->input('ip_litres_per_day_range_to');
        $ip_pump_head_operation = $request->input('ip_pump_head_operation');
        $ip_pump_head = $request->input('ip_pump_head');
        $ip_refrigerant = $request->input('ip_refrigerant');

        if($ip_application_type == 'aw') {
            $report = DB::select("select m.model_id,
                                        m.model_name,
                                        c.heating_capacity_pc,
                                        c.compressor_absorbed_power_pa,
                                        '' cooling_capacity_pf,
                                        m.hot_hot_water_pump_head,
                                        m.gen_refrigerant,
                                        m.gen_hot_water_quantity,
                                        round(c.heating_capacity_pc * 1000 * 0.86 / 30, 2) hot_water_quantity_l_per_day_calc
                                from aw_model_master m 
                                join aw_capacity_chart c on m.model_id = c.model_id
                                where 1 = 1 
                                    and c.hot_water_outlet_temperature = $ip_output_temperature
                                    and c.ambient_temperature = $ip_ambient_temperature
                                    and m.gen_refrigerant = case '$ip_refrigerant' when '' then m.gen_refrigerant else '$ip_refrigerant' end
                                    and (('$ip_litres_per_day_range_from' = '')
                                        or ('$ip_litres_per_day_range_from' != '' and round(c.heating_capacity_pc * 1000 * 0.86 / 30, 0) between '$ip_litres_per_day_range_from' and '$ip_litres_per_day_range_to')
                                        )
                                    and ( ('$ip_pump_head_operation' = '')
                                        or ('$ip_pump_head_operation' = '<=' and m.hot_hot_water_pump_head <= '$ip_pump_head')
                                        or ('$ip_pump_head_operation' = '>=' and m.hot_hot_water_pump_head >= '$ip_pump_head')
                                        )
                                order by c.heating_capacity_pc, 
                                        c.compressor_absorbed_power_pa"
                            );
        }else{
            $report = DB::select("select m.model_id,
                                        m.model_name,
                                        c.heating_capacity_pc,
                                        c.compressor_absorbed_power_pa,
                                        c.cooling_capacity_pf,
                                        m.hot_hot_water_pump_head,
                                        m.gen_refrigerant,
                                        m.gen_hot_water_quantity,
                                        round(c.heating_capacity_pc * 1000 * 0.86 / 30, 2) hot_water_quantity_l_per_day_calc
                                from ww_model_master m 
                                join ww_capacity_chart c on m.model_id = c.model_id
                                                        and m.gen_hot_water_outlet_temperature = c.hot_water_outlet_temperature 
                                                        and m.gen_chilled_water_outlet_temperature = c.chilled_water_outlet_temperature
                                where 1 = 1 
                                    and c.hot_water_outlet_temperature = $ip_output_temperature
                                    and c.chilled_water_outlet_temperature = $ip_ambient_temperature
                                    and m.gen_refrigerant = case '$ip_refrigerant' when '' then m.gen_refrigerant else '$ip_refrigerant' end
                                    and (('$ip_litres_per_day_range_from' = '')
                                        or ('$ip_litres_per_day_range_from' != '' and round(c.heating_capacity_pc * 1000 * 0.86 / 30, 0) between '$ip_litres_per_day_range_from' and '$ip_litres_per_day_range_to')
                                        )
                                    and ( ('$ip_pump_head_operation' = '')
                                        or ('$ip_pump_head_operation' = '<=' and m.hot_hot_water_pump_head <= '$ip_pump_head')
                                        or ('$ip_pump_head_operation' = '>=' and m.hot_hot_water_pump_head >= '$ip_pump_head')
                                        )
                                order by c.heating_capacity_pc, 
                                        c.chilled_water_outlet_temperature"
                                );
        }

        return response()->json([
                'message'=>'success',
                'report_details'=> $report
        ],200);

    }

    public function addSpecification(Request $request)
    {
        //$user_id = auth()->user()->id;
        //$user_type = auth()->user()->type;
        //$user_id = $request->user_id;
        //$user_type = $request->user_type;
        $url_key = '';
        $url_params = json_encode($request->all());
        $date_created = date("Y-m-d h:i:s");

        //$url_params = json_encode($request->all());

        $url_id = DB::table('url_keys')->insertGetId([
                    'user_id' => $request->input('user_id'),
                    'user_type' => $request->input('user_type'),
                    'url_params' => $url_params,
                    'date_created' => $date_created
                  ]);

        $key_details = DB::table('url_keys')->selectRaw('id, date_created')->where('id',$url_id)->get();

        $url_key = md5($key_details[0]->id).md5($key_details[0]->date_created);

        DB::table('url_keys')->where('id', $url_id)->update([
            'url_key' => $url_key
        ]);

        return response()->json([
                'message'=>'success',
                'key'=> $url_key
        ],200);
    }
    
    public function getSpecification(Request $request, $id)
    {
        $key_params = DB::table('url_keys')
                          ->selectRaw('url_params, DATE_FORMAT(date_created, "%D %M, %Y") date_created')
                          ->where('url_key', $id)
                          ->get();
        
        $date_created = $key_params[0]->date_created;

        $key_params = json_decode($key_params[0]->url_params);

        $user_id = $key_params->user_id;
        $user_type = $key_params->user_type;
        $ip_ambient_temperature = $key_params->ip_ambient_temperature;
        $ip_output_temperature = $key_params->ip_output_temperature;

        $client_id = $key_params->client_id;
        $client_name = $key_params->client_name;
        $consultant_id = $key_params->consultant_id;
        $consultant_name = $key_params->consultant_name;
        $contractor_id = $key_params->contractor_id;
        $contractor_name = $key_params->contractor_name;
        $employee_id = $key_params->employee_id;
        $employee_name = $key_params->employee_name; 

        $model_id = $key_params->model_id;
        //$model_name = $key_params->model_name;
        $heating_capacity_pc = $key_params->heating_capacity_pc;
        $compressor_absorbed_power_pa = $key_params->compressor_absorbed_power_pa;

        $application_type = $key_params->application_type;

        if($application_type == 'aw'){
            $model_details = DB::table("aw_model_master")
                                 ->select("*")
                                 ->where("model_id", $model_id)
                                 ->get();

            return view('selection.air_to_water')
                ->with([
                    'ip_ambient_temperature' => $ip_ambient_temperature, 
                    'ip_output_temperature' => $ip_output_temperature, 
                    'client_name' => $client_name,
                    'consultant_name' => $consultant_name,
                    'contractor_name' => $contractor_name,
                    'employee_name' => $employee_name,
                    //'model_name' => $model_name,
                    'model_description' => $model_details[0]->model_description,
                    'image_url' => $model_details[0]->image_url,
                    'heating_capacity_pc' => $heating_capacity_pc,
                    'compressor_absorbed_power_pa' => $compressor_absorbed_power_pa, 
                    'date_created' => $date_created,
                    'model_details' => $model_details[0]
                ]);

        }else{
            $model_details = DB::table("ww_model_master")
                                 ->select("*")
                                 ->where("model_id", $model_id)
                                 ->get();

            return view('selection.water_to_water')
                ->with([
                    'ip_ambient_temperature' => $ip_ambient_temperature, 
                    'ip_output_temperature' => $ip_output_temperature, 
                    'client_name' => $client_name,
                    'consultant_name' => $consultant_name,
                    'contractor_name' => $contractor_name,
                    'employee_name' => $employee_name,
                    //'model_name' => $model_name,
                    'model_description' => $model_details[0]->model_description,
                    'image_url' => $model_details[0]->image_url,
                    'heating_capacity_pc' => $heating_capacity_pc,
                    'compressor_absorbed_power_pa' => $compressor_absorbed_power_pa, 
                    'date_created' => $date_created,
                    'model_details' => $model_details[0]
                ]);

        }
        
    }
}

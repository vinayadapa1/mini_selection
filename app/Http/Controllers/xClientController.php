<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class ClientController extends Controller
{
    public function index(Request $request)
	{
		$client_details = DB::table('client_master')
		                      ->select('*')
							  ->where('status', 'active')
							  ->get();
							  
		return view('client', compact('client_details'));
	}
}

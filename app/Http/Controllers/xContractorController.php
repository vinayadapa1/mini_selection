<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class ContractorController extends Controller
{
    public function index(Request $request)
	{
		$contractor_details = DB::table('contractor_master')
		                      ->select('*')
							  ->where('status', 'active')
							  ->get();
							  
		return view('contractor', compact('contractor_details'));
	}
}

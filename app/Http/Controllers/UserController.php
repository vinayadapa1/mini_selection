<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

use Hash;

use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the users
        $user_details = User::all();

        // load the view and pass the users
        return view('user.index')
            ->with('user_details', $user_details);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $validator = $request->validate([
            'name'          => 'required',
            'mobile_no'     => 'required|numeric|digits:10',
            'email_id'      => 'required|email',
            'password'      => 'required',
            'type'          => 'required',
            //'expiry_date'   => 'required'
        ], [
            'name.required'         => 'Please enter name',
            'mobile_no.required'    => 'Please enter mobile no.',
            'mobile_no.digits'      => 'please enter valid mobile no',
            'email_id.required'     => 'Please enter email id',
            'email_id.email'        => 'Please enter valid email id',
            'password.required'     => 'Please enter password',
            'type.required'         => 'please select user type',
            //'expiry_date.required'  => 'please select expiry date'
        ]);

        // process the user addition
        /*if ($validator->fails()) {
            return redirect('sales-employee/create')
                ->withErrors($validator)
                ->withInput();
        } else {*/
            // store
            $user = new User;
            $user->name                 = $request->input('name');
            $user->mobile_no            = $request->input('mobile_no');
            $user->email                = $request->input('email_id');
			$user->password             = Hash::make($request->input('password'));
            $user->type                 = $request->input('type');
			//$user->expiry_date          = $request->input('expiry_date');
			$user->status               = 'active';
			$user->created_at           = date("Y-m-d h:i:s");
            $user->updated_at           = date("Y-m-d h:i:s");

            $user->save();

            // redirect
            Session::flash('message', 'Successfully created User!');
            return redirect('users');
        //}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the employee
        $user= User::find($id);

        // show the edit form and pass the shark
        return view('user.edit')
            ->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $validator = $request->validate([
            'name'          => 'required',
            'mobile_no'     => 'required|numeric|digits:10',
            'email_id'      => 'required|email',
            //'password'      => 'required',
            'type'          => 'required',
            //'expiry_date'   => 'required'
        ], [
            'name.required'         => 'Please enter name',
            'mobile_no.required'    => 'Please enter mobile no.',
            'mobile_no.digits'      => 'please enter valid mobile no',
            'email_id.required'     => 'Please enter email id',
            'email_id.email'        => 'Please enter valid email id',
            //'password.required'     => 'Please enter password',
            'type.required'         => 'please select user type',
            //'expiry_date.required'  => 'please select expiry date'
        ]);

        // process the login
        /*if ($validator->fails()) {
            return redirect('sales-employee/create')
                ->withErrors($validator)
                ->withInput();
        } else {*/
            // store
            $user = User::find($id);
            $user->name                 = $request->input('name');
            $user->mobile_no            = $request->input('mobile_no');
            $user->email                = $request->input('email_id');
			if($request->input('password') != ''){
                $user->password             = Hash::make($request->input('password'));
            }
            $user->type                 = $request->input('type');
			//$user->expiry_date          = $request->input('expiry_date');
			//$user->status               = 'active';
			//$user->created_at           = date("Y-m-d h:i:s");
            $user->updated_at           = date("Y-m-d h:i:s");

            $user->save();

            // redirect
            Session::flash('message', 'Successfully updated User!');
            return redirect('users');
        //}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $user = User::find($id);
        $user->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the user!');
        return redirect('users');
    }
}

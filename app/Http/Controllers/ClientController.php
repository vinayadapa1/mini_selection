<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;

use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the client
        $client_details = Client::all();

        // load the view and pass the employee
        return view('client.index')
            ->with('client_details', $client_details);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $validator = $request->validate([
            'first_name'       => 'required',
            'last_name'        => 'required',
            'contact_no'       => 'required|numeric|digits:10'
            /*'extention'        => 'required',
            'email_id'         => 'required',
            'region'           => 'required',
            'department'       => 'required',
            'city'             => 'required',
            'state'            => 'required',
            'country'          => 'required',
            'pincode'          => 'required',
            'address'          => 'required'*/
        ], [
            'first_name.required'       => 'please enter first name',
            'last_name.required'        => 'please enter last name',
            'contact_no.required'       => 'please enter contact no',
            'contact_no.digits'          => 'please enter valid contact no'
            /*'extention.required'        => 'please enter extention',
            'email_id.required'         => 'please enter email_id',
            'region.required'           => 'please enter region',
            'department.required'       => 'please enter department',
            'city.required'             => 'please enter city',
            'state.required'            => 'please enter state',
            'country.required'          => 'please enter country',
            'pincode.required'          => 'please enter pincode',
            'address.required'          => 'please enter address'*/
        ]);

        // process the login
        /*if ($validator->fails()) {
            return redirect('sales-employee/create')
                ->withErrors($validator)
                ->withInput();
        } else {*/
            // store
            $client = new Client;
            $client->first_name           = $request->input('first_name');
            $client->last_name            = $request->input('last_name');
            $client->contact_no           = $request->input('contact_no');
			$client->extention            = $request->input('extention');
			$client->email_id             = $request->input('email_id');
			$client->region               = $request->input('region');
			$client->department           = $request->input('department');
			$client->city                 = $request->input('city');
			$client->state                = $request->input('state');
			$client->country              = $request->input('country');
			$client->pincode              = $request->input('pincode');
			$client->address              = $request->input('address');
			$client->status               = 'active';
			$client->created_at           = date("Y-m-d h:i:s");
            $client->updated_at           = date("Y-m-d h:i:s");
			$client->created_by           = auth()->user()->id;

            $client->save();

            // redirect
            Session::flash('message', 'Successfully created Client!');
            return redirect('clients');
        //}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the employee
        $client= Client::find($id);

        // show the edit form and pass the shark
        return view('client.edit')
            ->with('client', $client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $validator = $request->validate([
            'first_name'       => 'required',
            'last_name'        => 'required',
            'contact_no'       => 'required|numeric|digits:10'
            /*'extention'        => 'required',
            'email_id'         => 'required',
            'region'           => 'required',
            'department'       => 'required',
            'city'             => 'required',
            'state'            => 'required',
            'country'          => 'required',
            'pincode'          => 'required',
            'address'          => 'required'*/
        ], [
            'first_name.required'       => 'please enter first name',
            'last_name.required'        => 'please enter last name',
            'contact_no.required'       => 'please enter contact no',
            'contact_no.digits'          => 'please enter valid contact no'
            /*'extention.required'        => 'please enter extention',
            'email_id.required'         => 'please enter email_id',
            'region.required'           => 'please enter region',
            'department.required'       => 'please enter department',
            'city.required'             => 'please enter city',
            'state.required'            => 'please enter state',
            'country.required'          => 'please enter country',
            'pincode.required'          => 'please enter pincode',
            'address.required'          => 'please enter address'*/
        ]);

        // process the login
        /*if ($validator->fails()) {
            return Redirect::to('sales-employee/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        } else {*/
            // store
            $client = Client::find($id);
            $client->first_name           = $request->input('first_name');
            $client->last_name            = $request->input('last_name');
            $client->contact_no           = $request->input('contact_no');
			$client->extention            = $request->input('extention');
			$client->email_id             = $request->input('email_id');
			$client->region               = $request->input('region');
			$client->department           = $request->input('department');
			$client->city                 = $request->input('city');
			$client->state                = $request->input('state');
			$client->country              = $request->input('country');
			$client->pincode              = $request->input('pincode');
			$client->address              = $request->input('address');
			//$client->status               = 'active';
			//$client->created_at           = date("Y-m-d h:i:s");
            $client->updated_at           = date("Y-m-d h:i:s");
			//$client->created_by           = auth()->user()->id;

            $client->save();

            // redirect
            Session::flash('message', 'Successfully updated client!');
            return redirect('clients');
        //}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $client = Client::find($id);
        $client->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the client!');
        return redirect('clients');
    }
}

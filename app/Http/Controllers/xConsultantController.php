<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class ConsultantController extends Controller
{
    public function index(Request $request)
	{
		$consultant_details = DB::table('consultant_master')
		                      ->select('*')
							  ->where('status', 'active')
							  ->get();
							  
		return view('consultant', compact('consultant_details'));
	}
}

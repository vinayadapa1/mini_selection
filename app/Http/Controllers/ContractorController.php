<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;

use App\Models\Contractor;
use Illuminate\Http\Request;

class ContractorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the contractor
        $contractor_details = Contractor::all();

        // load the view and pass the contractor
        return view('contractor.index')
            ->with('contractor_details', $contractor_details);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contractor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $validator = $request->validate([
            'first_name'       => 'required',
            'last_name'        => 'required',
            'contact_no'       => 'required|numeric|digits:10'
            /*'extention'        => 'required',
            'email_id'         => 'required',
            'region'           => 'required',
            'department'       => 'required',
            'city'             => 'required',
            'state'            => 'required',
            'country'          => 'required',
            'pincode'          => 'required',
            'address'          => 'required'*/
        ], [
            'first_name.required'       => 'please enter first name',
            'last_name.required'        => 'please enter last name',
            'contact_no.required'       => 'please enter contact no',
            'contact_no.digits'          => 'please enter valid contact no'
            /*'extention.required'        => 'please enter extention',
            'email_id.required'         => 'please enter email_id',
            'region.required'           => 'please enter region',
            'department.required'       => 'please enter department',
            'city.required'             => 'please enter city',
            'state.required'            => 'please enter state',
            'country.required'          => 'please enter country',
            'pincode.required'          => 'please enter pincode',
            'address.required'          => 'please enter address'*/
        ]);

        // process the login
        /*if ($validator->fails()) {
            return redirect('sales-employee/create')
                ->withErrors($validator)
                ->withInput();
        } else {*/
            // store
            $contractor = new Contractor;
            $contractor->first_name           = $request->input('first_name');
            $contractor->last_name            = $request->input('last_name');
            $contractor->contact_no           = $request->input('contact_no');
			$contractor->extention            = $request->input('extention');
			$contractor->email_id             = $request->input('email_id');
			$contractor->region               = $request->input('region');
			$contractor->department           = $request->input('department');
			$contractor->city                 = $request->input('city');
			$contractor->state                = $request->input('state');
			$contractor->country              = $request->input('country');
			$contractor->pincode              = $request->input('pincode');
			$contractor->address              = $request->input('address');
			$contractor->status               = 'active';
			$contractor->created_at           = date("Y-m-d h:i:s");
            $contractor->updated_at           = date("Y-m-d h:i:s");
			$contractor->created_by           = auth()->user()->id;

            $contractor->save();

            // redirect
            Session::flash('message', 'Successfully created Contractor!');
            return redirect('contractors');
        //}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contractor  $contractor
     * @return \Illuminate\Http\Response
     */
    public function show(Contractor $contractor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contractor  $contractor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the consultant
        $contractor= Contractor::find($id);

        // show the edit form and pass the shark
        return view('contractor.edit')
            ->with('contractor', $contractor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contractor  $contractor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $validator = $request->validate([
            'first_name'       => 'required',
            'last_name'        => 'required',
            'contact_no'       => 'required|numeric|digits:10'
            /*'extention'        => 'required',
            'email_id'         => 'required',
            'region'           => 'required',
            'department'       => 'required',
            'city'             => 'required',
            'state'            => 'required',
            'country'          => 'required',
            'pincode'          => 'required',
            'address'          => 'required'*/
        ], [
            'first_name.required'       => 'please enter first name',
            'last_name.required'        => 'please enter last name',
            'contact_no.required'       => 'please enter contact no',
            'contact_no.digits'          => 'please enter valid contact no'
            /*'extention.required'        => 'please enter extention',
            'email_id.required'         => 'please enter email_id',
            'region.required'           => 'please enter region',
            'department.required'       => 'please enter department',
            'city.required'             => 'please enter city',
            'state.required'            => 'please enter state',
            'country.required'          => 'please enter country',
            'pincode.required'          => 'please enter pincode',
            'address.required'          => 'please enter address'*/
        ]);

        // process the login
        /*if ($validator->fails()) {
            return Redirect::to('sales-employee/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        } else {*/
            // store
            $contractor = Contractor::find($id);
            $contractor->first_name           = $request->input('first_name');
            $contractor->last_name            = $request->input('last_name');
            $contractor->contact_no           = $request->input('contact_no');
			$contractor->extention            = $request->input('extention');
			$contractor->email_id             = $request->input('email_id');
			$contractor->region               = $request->input('region');
			$contractor->department           = $request->input('department');
			$contractor->city                 = $request->input('city');
			$contractor->state                = $request->input('state');
			$contractor->country              = $request->input('country');
			$contractor->pincode              = $request->input('pincode');
			$contractor->address              = $request->input('address');
			//$contractor->status               = 'active';
			//$contractor->created_at           = date("Y-m-d h:i:s");
            $contractor->updated_at           = date("Y-m-d h:i:s");
			//$contractor->created_by           = auth()->user()->id;

            $contractor->save();

            // redirect
            Session::flash('message', 'Successfully updated contractor!');
            return redirect('contractors');
        //}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contractor  $contractor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $contractor = Contractor::find($id);
        $contractor->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the contractor!');
        return redirect('contractors');
    }
}

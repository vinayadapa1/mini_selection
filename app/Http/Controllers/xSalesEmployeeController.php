<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class SalesEmployeeController extends Controller
{
    public function index(Request $request)
	{
		$employee_details = DB::table('sales_employee_master')
		                      ->select('*')
							  ->where('status', 'active')
							  ->get();
							  
		return view('sales_employee', compact('employee_details'));
	}
	
	public function add(Request $request)
	{
		if ($request->input('add_update_employee') == 'add_update'){
			
			$first_name = $request->input('first_name');
			$last_name = $request->input('last_name');
			$contact_no = $request->input('contact_no');
			$extention = $request->input('extention');
			$email_id = $request->input('email_id');
			$region = $request->input('region');
			$department = $request->input('department');
			$city = $request->input('city');
			$state = $request->input('state');
			$country = $request->input('country');
			$pincode = $request->input('pincode');
			$address = $request->input('address');
			$status = 'active';
			$created_on = date("Y-m-d h:i:s");
			$created_by = auth()->user()->id;
			
			if($request->input('employee_id') == '')
			{
				DB::table('sales_employee_master')->insert([
				                                            'first_name' => $first_name,
                                                            'last_name' => $last_name,
															'contact_no' => $contact_no,
                                                            'extention' => $extention,
                                                            'email_id' => $email_id,
                                                            'region' =>	$region,
                                                            'department' => $department,
                                                            'city' => $city,	
                                                            'state' => $state,			
                                                            'country' => $country,
                                                            'pincode' => $pincode,
                                                            'address' => $address,	
                                                            'status' =>	$status,
                                                            'created_on' => $created_on,
                                                            'created_by' =>	$created_by														
														   ]);
			}else{
				DB::table('sales_employee_master')->insert([
				                                            'first_name' => $first_name,
                                                            'last_name' => $last_name,
															'contact_no' => $contact_no,
                                                            'extention' => $extention,
                                                            'email_id' => $email_id,
                                                            'region' =>	$region,
                                                            'department' => $department,
                                                            'city' => $city,	
                                                            'state' => $state,			
                                                            'country' => $country,
                                                            'pincode' => $pincode,
                                                            'address' => $address														
														   ]);
														   
														   
			}
			return redirect('sales-employee');
		}else{
			return view('add_sales_employee');
		}
		
		/*$data = (object)array("p_id" =>"", "first_name"=>"", "last_name" => "");
		$employee_id = $request->input('employee_id');
		dd($employee_id);
		if($request->input('employee_id') != '' && $request->input('add_update_employee')=="")
		{
			$data = DB::table('sales_employee_master')
			          ->select('*')
					  ->where('id', 1)
					  ->first();
		}
		
		return view('add_sales_employee',compact(['data']));*/
		
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;

use App\Models\Consultant;
use Illuminate\Http\Request;

class ConsultantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the client
        $consultant_details = Consultant::all();

        // load the view and pass the employee
        return view('consultant.index')
            ->with('consultant_details', $consultant_details);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('consultant.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $validator = $request->validate([
            'first_name'       => 'required',
            'last_name'        => 'required',
            'contact_no'       => 'required|numeric|digits:10'
            /*'extention'        => 'required',
            'email_id'         => 'required',
            'region'           => 'required',
            'department'       => 'required',
            'city'             => 'required',
            'state'            => 'required',
            'country'          => 'required',
            'pincode'          => 'required',
            'address'          => 'required'*/
        ], [
            'first_name.required'       => 'please enter first name',
            'last_name.required'        => 'please enter last name',
            'contact_no.required'       => 'please enter contact no',
            'contact_no.digits'          => 'please enter valid contact no'
            /*'extention.required'        => 'please enter extention',
            'email_id.required'         => 'please enter email_id',
            'region.required'           => 'please enter region',
            'department.required'       => 'please enter department',
            'city.required'             => 'please enter city',
            'state.required'            => 'please enter state',
            'country.required'          => 'please enter country',
            'pincode.required'          => 'please enter pincode',
            'address.required'          => 'please enter address'*/
        ]);

        // process the login
        /*if ($validator->fails()) {
            return redirect('sales-employee/create')
                ->withErrors($validator)
                ->withInput();
        } else {*/
            // store
            $consultant = new Consultant;
            $consultant->first_name           = $request->input('first_name');
            $consultant->last_name            = $request->input('last_name');
            $consultant->contact_no           = $request->input('contact_no');
			$consultant->extention            = $request->input('extention');
			$consultant->email_id             = $request->input('email_id');
			$consultant->region               = $request->input('region');
			$consultant->department           = $request->input('department');
			$consultant->city                 = $request->input('city');
			$consultant->state                = $request->input('state');
			$consultant->country              = $request->input('country');
			$consultant->pincode              = $request->input('pincode');
			$consultant->address              = $request->input('address');
			$consultant->status               = 'active';
			$consultant->created_at           = date("Y-m-d h:i:s");
            $consultant->updated_at           = date("Y-m-d h:i:s");
			$consultant->created_by           = auth()->user()->id;

            $consultant->save();

            // redirect
            Session::flash('message', 'Successfully created Consultant!');
            return redirect('consultants');
        //}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Consultant  $consultant
     * @return \Illuminate\Http\Response
     */
    public function show(Consultant $consultant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Consultant  $consultant
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the consultant
        $consultant= Consultant::find($id);

        // show the edit form and pass the shark
        return view('consultant.edit')
            ->with('consultant', $consultant);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Consultant  $consultant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $validator = $request->validate([
            'first_name'       => 'required',
            'last_name'        => 'required',
            'contact_no'       => 'required|numeric|digits:10'
            /*'extention'        => 'required',
            'email_id'         => 'required',
            'region'           => 'required',
            'department'       => 'required',
            'city'             => 'required',
            'state'            => 'required',
            'country'          => 'required',
            'pincode'          => 'required',
            'address'          => 'required'*/
        ], [
            'first_name.required'       => 'please enter first name',
            'last_name.required'        => 'please enter last name',
            'contact_no.required'       => 'please enter contact no',
            'contact_no.digits'          => 'please enter valid contact no'
            /*'extention.required'        => 'please enter extention',
            'email_id.required'         => 'please enter email_id',
            'region.required'           => 'please enter region',
            'department.required'       => 'please enter department',
            'city.required'             => 'please enter city',
            'state.required'            => 'please enter state',
            'country.required'          => 'please enter country',
            'pincode.required'          => 'please enter pincode',
            'address.required'          => 'please enter address'*/
        ]);

        // process the login
        /*if ($validator->fails()) {
            return Redirect::to('sales-employee/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        } else {*/
            // store
            $consultant = Consultant::find($id);
            $consultant->first_name           = $request->input('first_name');
            $consultant->last_name            = $request->input('last_name');
            $consultant->contact_no           = $request->input('contact_no');
			$consultant->extention            = $request->input('extention');
			$consultant->email_id             = $request->input('email_id');
			$consultant->region               = $request->input('region');
			$consultant->department           = $request->input('department');
			$consultant->city                 = $request->input('city');
			$consultant->state                = $request->input('state');
			$consultant->country              = $request->input('country');
			$consultant->pincode              = $request->input('pincode');
			$consultant->address              = $request->input('address');
			//$consultant->status               = 'active';
			//$consultant->created_at           = date("Y-m-d h:i:s");
            $consultant->updated_at           = date("Y-m-d h:i:s");
			//$consultant->created_by           = auth()->user()->id;

            $consultant->save();

            // redirect
            Session::flash('message', 'Successfully updated consultant!');
            return redirect('consultants');
        //}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Consultant  $consultant
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $consultant = Consultant::find($id);
        $consultant->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the consultant!');
        return redirect('consultants');
    }
}
